"""Register models from the app users to admin panel."""
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import AthleteFile, Offer


class CustomUserAdmin(UserAdmin):
    """Custom class for user data in admin panel"""
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = get_user_model()
    fieldsets = UserAdmin.fieldsets + ((None, {"fields": ("coach",)}),)
    add_fieldsets = UserAdmin.add_fieldsets +\
                    ((None,
                      {"fields":(
                          'coach',
                          'phone_number',
                          'street_address',
                          'country',
                          'email',
                          'city',)}),)

admin.site.register(get_user_model(), CustomUserAdmin)
admin.site.register(Offer)
admin.site.register(AthleteFile)
