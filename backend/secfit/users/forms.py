"""Custom forms for the app users"""
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import get_user_model


class CustomUserCreationForm(UserCreationForm):
    """
    Custom form for creating user with fields:
    - phone_number
    - country
    - city
    - street_address
    """
    phone_number = forms.CharField(max_length=50)
    country = forms.CharField(max_length=50)
    city = forms.CharField(max_length=50)
    street_address = forms.CharField(max_length=50)

    class Meta(UserCreationForm):
        model = get_user_model()
        fields = ("username", "coach", "phone_number", "country", "city", "street_address")


class CustomUserChangeForm(UserChangeForm):
    """
    Custom form for changing users fields:
    - username
    - coach
    """

    class Meta:
        model = get_user_model()
        fields = ("username", "coach")
