"""Serializers for models in app users."""
from django import forms
from django.contrib.auth import password_validation
from rest_framework import serializers

from users.models import AthleteFile, Offer, User

class UserSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for the custom user model."""
    password = serializers.CharField(style={"input_type": "password"}, write_only=True)
    password1 = serializers.CharField(style={"input_type": "password"}, write_only=True)

    class Meta:
        model = User
        fields = [
            "url",
            "id",
            "email",
            "username",
            "password",
            "password1",
            "athletes",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach",
            "workouts",
            "coach_files",
            "athlete_files",
        ]


    def validate_password(self, value=None):
        """
        Uses django.contrib.auths password_validation.

        :raises serializers.ValidationError: If password is not valid
        """
        data = self.get_initial()
        password = data.get("password")

        try:
            password_validation.validate_password(password)
        except forms.ValidationError as error:
            raise serializers.ValidationError(error.messages)

    def create(self, validated_data):
        username = validated_data["username"]
        email = validated_data["email"]
        password = validated_data["password"]
        phone_number = validated_data["phone_number"]
        country = validated_data["country"]
        city = validated_data["city"]
        street_address = validated_data["street_address"]
        user_obj = User.objects.create(
            username=username,
            email=email,
            phone_number=phone_number,
            country=country, city=city,
            street_address=street_address)
        user_obj.set_password(password)
        user_obj.save()

        return user_obj


class UserGetSerializer(serializers.HyperlinkedModelSerializer): # pragma: no cover
    """Custom serializer to get detail of specific user."""
    class Meta:
        model = User
        fields = [
            "url",
            "id",
            "email",
            "username",
            "athletes",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach",
            "workouts",
            "coach_files",
            "athlete_files",
        ]


class UserPutSerializer(serializers.ModelSerializer): # pragma: no cover
    """Custom serializer to update an users athletes"""
    class Meta:
        model = User
        fields = ["athletes"]

    def update(self, instance, validated_data):
        athletes_data = validated_data["athletes"]
        instance.athletes.set(athletes_data)

        return instance


class AthleteFileSerializer(serializers.HyperlinkedModelSerializer): # pragma: no cover
    """Serializer for athlete files."""
    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        model = AthleteFile
        fields = ["url", "id", "owner", "file", "athlete"]

    def create(self, validated_data):
        return AthleteFile.objects.create(**validated_data)


class OfferSerializer(serializers.HyperlinkedModelSerializer): # pragma: no cover
    """Serializer for offers."""
    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        model = Offer
        fields = [
            "url",
            "id",
            "owner",
            "recipient",
            "status",
            "timestamp",
        ]
