"""
Testfile for two-way testing of register new user.
"""
import random
import string

from django.test import Client, TestCase
from rest_framework import status

from .testcases import test_cases

def generate_valid_username(length=100):
    """Generates and returns a valid username of length :length:"""
    allowed_chars = string.ascii_letters + string.digits + '_@.+-'
    generated_username = ""
    for _ in range(length):
        random_char = random.choice(allowed_chars)
        generated_username = generated_username.join(random_char)

    return generated_username

test_data = {
    'valid': {
        'username': 'to_be_created',
        'email': 'mail@test.com',
        'password': 'some_password_123',
        'password1': 'some_password_123',
        'phone_number': '88855888',
        'country': 'Norge',
        'city': 'Trondheim',
        'street_address': 'O.S. Bragstads plass 1',
    }, 
    'invalid': {
        'username': '{invalid_name}',
        'email': 'a@a',
    }, 
    'blank':{
        'username': '',
        'email': '',
        'password': '',
        'password1': '',
        'phone_number': '',
        'country': '',
        'city': '',
        'street_address': '',
    }
}


class TwoWayTestCase(TestCase):
    """
    2-way testing of register user.
    See testcases.py for the generated cases.
    ----------------------------------------
    username       - [valid, invalid, blank]
    email          - [valid, invalid, blank]
    password       - [valid, blank]
    password1      - [valid, blank]
    phone_number   - [valid, blank]
    country        - [valid, blank]
    city           - [valid, blank]
    street_address - [valid, blank]
    """

    def test_two_way_register(self):
        """Test register of user pairwise through http responses"""
        client = Client()
        url = '/api/users/'
        # test_cases imported from .testcases
        for case in test_cases:
            context = {}

            # Convert to actual data
            for key, input_value in case.items():
                context[key] = test_data[input_value][key]
                # Generate valid username, to avoid duplicate usernames
                if key == "username" and input_value == "valid":
                    context[key] = generate_valid_username()

            response = client.post(url, context)

            # Logic to set 201_created with the given requirements
            username_valid = case["username"] == "valid"
            password_valid = case["password"] == "valid" and case["password1"] == "valid"
            email_valid = case["email"] == "valid" or case["email"] == "blank"
            if username_valid and password_valid and email_valid:
                self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            else:
                self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
