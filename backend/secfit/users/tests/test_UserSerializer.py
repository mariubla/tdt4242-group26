"""
Testfile for statement coverage of UserSerializer.
"""
from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from rest_framework import serializers, status
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory, APITestCase

from ..serializers import UserSerializer

USERNAME = "test_user"
EMAIL = "test@mail.no"
PHONENUMBER = "88855888"
COUNTRY = "Norway"
CITY = "Trondheim"
STREET_ADDRESS = "Street 123"

VALID_PASSWORD = "perfect_password_123"
INVALID_PASSWORD = "a"

USER_URL = "/api/users/"

User = get_user_model()


class UserSerializerTestCase(TestCase):
    """Testcases for testing the UserSerializer"""
    def setUp(self):
        """Set up an user for testing"""
        user = User(
            username=USERNAME,
            email=EMAIL,
            phone_number=PHONENUMBER,
            country=COUNTRY,
            city=CITY,
            street_address=STREET_ADDRESS,
        )
        user.set_password(VALID_PASSWORD)
        user.save()

    def get_serialized_user(self):
        """Fetches user from setUp and returns the serialized user"""
        test_user = User.objects.get(username=USERNAME)
        factory = APIRequestFactory()
        request = factory.get('/')

        context = {
            'request': Request(request),
        }

        serialized_user = UserSerializer(instance=test_user, context=context)
        return serialized_user.data

    def test_valid_password(self):
        data = {'password': VALID_PASSWORD}
        UserSerializer.validate_password(UserSerializer(data=data))

    def test_invalid_password(self):
        with self.assertRaises(serializers.ValidationError):
            error_data = {"password": INVALID_PASSWORD}
            UserSerializer.validate_password(UserSerializer(data=error_data))

    def test_field_email(self):
        serialized_data = self.get_serialized_user()
        self.assertEqual(serialized_data["email"], EMAIL)

    def test_field_phone_number(self):
        serialized_data = self.get_serialized_user()
        self.assertEqual(serialized_data["phone_number"], PHONENUMBER)

    def test_field_country(self):
        serialized_data = self.get_serialized_user()
        self.assertEqual(serialized_data["country"], COUNTRY)

    def test_field_city(self):
        serialized_data = self.get_serialized_user()
        self.assertEqual(serialized_data["city"], CITY)

    def test_field_street_address(self):
        serialized_data = self.get_serialized_user()
        self.assertEqual(serialized_data["street_address"], STREET_ADDRESS)

    def test_create(self):
        created_user_username = "test_create_user"
        data = {
            "username": created_user_username,
            "email": EMAIL,
            "phone_number": PHONENUMBER,
            "country": COUNTRY,
            "city": CITY,
            "street_address": STREET_ADDRESS,
            "password": VALID_PASSWORD,
        }
        created_user = UserSerializer.create(UserSerializer, validated_data=data)
        self.assertEqual(User.objects.get(username=created_user_username), created_user)

class RegisterBoundaryTestCase(APITestCase):
    def get_response(self, username=USERNAME, password=VALID_PASSWORD):
        """Helper function to get response for posting user with :username: and :password:"""
        client = Client()
        context = {
            'username': username,
            'password': password,
            'password1': password,
            'email': '',
            'phone_number': '',
            'country': '',
            'city': '',
            'street_address': ''
        }

        response = client.post(USER_URL, context)
        return response

    def test_username_inside(self):
        """Test username requirement of length >= 1"""
        username_inside = 'a'
        response = self.get_response(username=username_inside)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_username_outside(self):
        """Test username requirement of length >= 1"""
        username_outside = ''
        response = self.get_response(username=username_outside)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_password_inside(self):
        """Test password requirements of length >= 2"""
        password_inside = 'aa'
        response = self.get_response(password=password_inside)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_password_outside(self):
        """Test password requirements of length >= 2"""
        password_outside = 'a'
        response = self.get_response(password=password_outside)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)