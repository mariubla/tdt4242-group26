"""Custom permission classes used in the app users."""
from rest_framework import permissions
from .models import User


class IsCurrentUser(permissions.BasePermission):
    """Checks if requesting user is the user they requests to access"""
    def has_object_permission(self, request, view, obj):
        return obj == request.user


class IsAthleteOfFile(permissions.BasePermission):
    """Checks if the requesting user is the athlete of the file"""
    def has_permission(self, request, view):
        if request.method == "POST":
            if request.data.get("athlete"):
                athlete_id = request.data["athlete"].split("/")[-2]
                return athlete_id == request.user.id
            return False

        return True

    def has_object_permission(self, request, view, obj):
        return request.user == obj.athlete


class IsCoachOfAthlete(permissions.BasePermission):
    """Checks if the requesting user is the coach of the files athlete"""
    def has_permission(self, request, view):
        if request.method == "POST":
            if request.data.get("athlete"):
                athlete_id = request.data["athlete"].split("/")[-2]
                try:
                    athlete = User.objects.get(pk=athlete_id)
                    return athlete.coach == request.user
                except User.DoesNotExist:
                    return False
            return False

        return True