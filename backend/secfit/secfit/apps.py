"""AppConfig for the base app secfit"""
from django.apps import AppConfig


class SecfitConfig(AppConfig):
    """AppConfig for secfit app

    Attributes:
        name (str): The name of the application
    """

    name = "secfit"
