"""Models for base app secfit."""
from django.db import models

class RememberMe(models.Model):
    """Django model for an remember_me cookie used for remember me functionality.

    Attributes:
        remember_me:        Value of cookie used for remember me
    """

    remember_me = models.CharField(max_length=500)

    def __str__(self):
        return self.remember_me
