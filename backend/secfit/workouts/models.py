"""Contains the models for the workouts Django application."""
from django.db import models
from users.models import User


class Workout(models.Model):
    """Django model for a workout that users can log.

    A workout has several attributes, and is associated with one or more exercises
    (instances) and, optionally, files uploaded by the user.

    Attributes:
        name:        Name of the workout
        date:        Date the workout was performed or is planned
        notes:       Notes about the workout
        owner:       User that logged the workout
        visibility:  The visibility level of the workout: Public, Coach, or Private
    """
    PUBLIC = "PU"
    COACH = "CO"
    PRIVATE = "PR"
    VISIBILITY_CHOICES = [
        (PUBLIC, "Public"),
        (COACH, "Coach"),
        (PRIVATE, "Private"),
    ]

    name = models.CharField(max_length=100)
    date = models.DateTimeField()
    notes = models.TextField()
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="workouts")
    visibility = models.CharField(max_length=2, choices=VISIBILITY_CHOICES, default=COACH)

    class Meta:
        ordering = ["-date"]

    def __str__(self):
        return self.name


class Exercise(models.Model):
    """Django model for an exercise type that users can create.

    Attributes:
        name:        Name of the exercise type
        description: Description of the exercise type
        unit:        Name of the unit for the exercise type (e.g., reps, seconds)
    """

    name = models.CharField(max_length=100)
    description = models.TextField()
    unit = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class ExerciseInstance(models.Model):
    """Django model for an instance of an exercise.

    Attributes:
        workout:    The workout associated with this exercise instance
        exercise:   The exercise type of this instance
        sets:       The number of sets the owner will perform/performed
        number:     The number of repetitions in each set the owner will perform/performed
    """

    workout = models.ForeignKey(Workout, on_delete=models.CASCADE, related_name="exercise_instances")
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE, related_name="instances")
    sets = models.IntegerField()
    number = models.IntegerField()


def workout_directory_path(instance, filename):
    """Return path for which workout files should be uploaded on the web server

    Args:
        instance (WorkoutFile): WorkoutFile instance
        filename (str): Name of the file

    Returns:
        str: Path where workout file is stored
    """
    return f"workouts/{instance.workout.id}/{filename}"


class WorkoutFile(models.Model):
    """Django model for file associated with a workout.

    Attributes:
        workout: The workout for which this file has been uploaded
        owner:   The user who uploaded the file
        file:    The actual file that's being uploaded
    """

    workout = models.ForeignKey(Workout, on_delete=models.CASCADE, related_name="files")
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="workout_files")
    file = models.FileField(upload_to=workout_directory_path)
