"""Tests for the workouts application."""
from datetime import date

from django.contrib.auth.models import AnonymousUser
from django.test import TestCase
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory
from users.models import User

from .models import Workout, WorkoutFile
from .permissions import (IsCoachOfWorkout, IsCoachToOwner, IsOwner,
                          IsOwnerOfWorkout, IsPublic, IsReadOnly,
                          IsWorkoutPublic)

WORKOUT_NAME = "Test workout"
OWNER_USERNAME = "owner"
NOT_OWNER_USERNAME = "not_owner"
COACH_USERNAME = "coach"

def get_request():
    """Creates and returns an API Request"""
    factory = APIRequestFactory()
    request = factory.get('/')
    return Request(request)

class PermissionTestCase(TestCase):
    """
    TestCase to test all class with functions in workouts/permissions.py.
    Goal: 100% Statement coverage
    """

    def setUp(self):
        # Set up owner
        owner = User(username=OWNER_USERNAME,
                     email='test@invalid.com')
        owner.set_password('some_123_password')
        owner.save()

        # Set up user
        not_owner = User(username=NOT_OWNER_USERNAME,
                         email='test_not_owner@invalid.com')
        not_owner.set_password('some_123_password')
        not_owner.save()

        # Set up coach
        coach = User(username=COACH_USERNAME,
                     email='coach@invalid.com')
        coach.set_password('some_123_password')
        coach.save()
        owner.coach = coach
        owner.save()

        # Set up workout
        workout = Workout.objects.create(
            name=WORKOUT_NAME,
            date=date.today(),
            owner=owner,
            notes="test notes",
            visibility="PU")

        # Set up workoutfile
        WorkoutFile.objects.create(workout=workout,
                                   owner=owner,
                                   file="media/workouts/{}/workoutfile".format(workout.id))

    def test_IsOwner(self):
        owner = User.objects.get(username=OWNER_USERNAME)
        workout = Workout.objects.get(name=WORKOUT_NAME)
        request = get_request()

        # Is owner
        request.user = owner
        is_owner = IsOwner.has_object_permission(self=IsOwner,
                                                 request=request,
                                                 view=None, obj=workout)
        self.assertTrue(is_owner)

        # Is not owner
        not_owner = User.objects.get(username=NOT_OWNER_USERNAME)
        request.user = not_owner
        is_owner = IsOwner.has_object_permission(self=IsOwner,
                                                 request=request,
                                                 view=None, obj=workout)
        self.assertFalse(is_owner)

        # Anonymous user
        request.user = AnonymousUser()
        is_owner = IsOwner.has_object_permission(self=IsOwner,
                                                 request=request,
                                                 view=None, obj=workout)
        self.assertFalse(is_owner)

    def test_IsOwnerOfWorkout(self):
        owner = User.objects.get(username=OWNER_USERNAME)
        not_owner = User.objects.get(username=NOT_OWNER_USERNAME)
        workoutfile = WorkoutFile.objects.filter(owner=owner).first()
        request = get_request()
        request.user = owner

        # Tests for method has_object_permission
        # Is owner
        is_owner = IsOwnerOfWorkout.has_object_permission(self=IsOwnerOfWorkout,
                                                          request=request,
                                                          view=None, obj=workoutfile)
        self.assertTrue(is_owner)

        # Is not owner
        request.user = not_owner
        is_owner = IsOwnerOfWorkout.has_object_permission(self=IsOwnerOfWorkout,
                                                          request=request,
                                                          view=None, obj=workoutfile)
        self.assertFalse(is_owner)

        # Tests for method has_permission
        # If not method="POST"
        request.user = not_owner
        request.method = "GET"
        request.data["workout"] = workoutfile.file.url
        is_owner = IsOwnerOfWorkout.has_permission(self=IsOwnerOfWorkout,
                                                   request=request, view=None)
        self.assertTrue(is_owner)

        # Is owner
        request.user = owner
        request.method = "POST"
        is_owner = IsOwnerOfWorkout.has_permission(self=IsOwnerOfWorkout,
                                                   request=request, view=None)
        self.assertTrue(is_owner)

        # Is not owner
        request.user = not_owner
        is_owner = IsOwnerOfWorkout.\
            has_permission(self=IsOwnerOfWorkout,
                           request=request, view=None)
        self.assertFalse(is_owner)

        # If workout not provided
        request.data["workout"] = None
        request.user = owner
        is_owner = IsOwnerOfWorkout.\
            has_permission(self=IsOwnerOfWorkout,
                           request=request, view=None)
        self.assertFalse(is_owner)

    def test_IsCoachAndVisibleToCoach(self):
        coach = User.objects.get(username=COACH_USERNAME)
        owner = User.objects.get(username=OWNER_USERNAME)
        workoutfile = WorkoutFile.objects.filter(owner=owner).first()

        request = get_request()
        request.user = coach
        # Test method has_object_permission
        has_obj_permission = IsCoachToOwner.\
            has_object_permission(self=IsCoachToOwner,
                                  request=request,
                                  view=None,
                                  obj=workoutfile)
        self.assertTrue(has_obj_permission)

    def test_IsCoachOfWorkoutAndVisibleToCoach(self):
        coach = User.objects.get(username=COACH_USERNAME)
        owner = User.objects.get(username=OWNER_USERNAME)
        request = get_request()
        request.user = coach
        workoutfile = WorkoutFile.objects.filter(owner=owner).first()
        has_obj_permission = IsCoachOfWorkout.\
            has_object_permission(self=IsCoachOfWorkout,
                                  request=request,
                                  view=None,
                                  obj=workoutfile)
        self.assertTrue(has_obj_permission)

    def test_IsPublic(self):
        workout = Workout.objects.get(name=WORKOUT_NAME)
        is_public = IsPublic.has_object_permission(
            self=IsPublic,
            request=None,
            view=None,
            obj=workout
        )
        self.assertTrue(is_public)

    def test_IsWorkoutPublic(self):
        owner = User.objects.get(username=OWNER_USERNAME)
        workoutfile = WorkoutFile.objects.filter(owner=owner).first()
        is_workout_public = IsWorkoutPublic.has_object_permission(
            self=None,
            request=IsWorkoutPublic,
            view=None,
            obj=workoutfile
        )
        self.assertTrue(is_workout_public)

    def test_IsReadOnly(self):
        request = get_request()
        is_read_only = IsReadOnly.has_object_permission(
            self=IsReadOnly,
            request=request,
            view=None,
            obj=None
        )
        self.assertTrue(is_read_only)
