"""
Url endpoints for the app comments.
"""
from django.urls import path

from comments.views import WorkoutCommentDetail, WorkoutCommentList

urlpatterns = [
    path("comments/", WorkoutCommentList.as_view(), name="workoutcomment-list"),
    path("comments/<int:pk>/", WorkoutCommentDetail.as_view(), name="workoutcomment-detail"),
]
