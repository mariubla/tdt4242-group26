"""
Serializers for models in the app comments.
"""
from rest_framework import serializers
from rest_framework.serializers import HyperlinkedRelatedField
from comments.models import WorkoutComment
from workouts.models import Workout


class WorkoutCommentSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source="owner.username")
    workout = HyperlinkedRelatedField(
        queryset=Workout.objects.all(), view_name="workout-detail"
    )

    class Meta:
        model = WorkoutComment
        fields = ["url", "id", "owner", "workout", "content", "timestamp"]
