"""
Views for the app comments.
"""
from django.db.models import Q
from rest_framework import generics, mixins, permissions
from rest_framework.filters import OrderingFilter
from workouts.permissions import IsOwner, IsReadOnly

from comments.models import WorkoutComment
from comments.permissions import IsCommentVisibleToUser
from comments.serializers import WorkoutCommentSerializer


class WorkoutCommentList(
        mixins.ListModelMixin,
        mixins.CreateModelMixin, 
        generics.GenericAPIView,
):
    """
    View to list and create WorkoutComments.
    """
    serializer_class = WorkoutCommentSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [OrderingFilter]
    ordering_fields = ["timestamp"]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        """A comment should be visible to the requesting user if any of the following hold:
            - The comment is on a public visibility workout
            - The comment was written by the user
            - The comment is on a coach visibility workout and the user is the workout owner's coach
            - The comment is on a workout owned by the user
        """
        queryset = WorkoutComment.objects.filter(
                Q(workout__visibility="PU")
                | Q(owner=self.request.user)
                | (Q(workout__visibility="CO")
                    & Q(workout__owner__coach=self.request.user))
                | Q(workout__owner=self.request.user)
            ).distinct()

        return queryset


class WorkoutCommentDetail(
        mixins.RetrieveModelMixin,
        generics.GenericAPIView,
):
    """
    View to get a specific WorkoutComment.
    """
    queryset = WorkoutComment.objects.all()
    serializer_class = WorkoutCommentSerializer
    permission_classes = [
        permissions.IsAuthenticated & IsCommentVisibleToUser & (IsOwner | IsReadOnly)
    ]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)