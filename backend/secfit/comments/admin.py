"""
Register models from the app comments to admin panel.
"""
from django.contrib import admin

from .models import WorkoutComment

admin.site.register(WorkoutComment)
