/**
 * Show confirmation dialog for deletion of user
 */
function showConfirmationDialog() {
    document.getElementById("confirm-deletion").classList.remove("hide");
}

/**
 * Hide confirmation dialog for deletion of user
 */
function hideConfirmationDialog() {
    document.getElementById("confirm-deletion").classList.add("hide");
}

/**
 * Send the delete user request and show correct view
 */
function deleteUser() {
    deleteUserRequest();
    document.getElementById("user-deleted").classList.remove("hide");
    hideConfirmationDialog();
}

/**
 * Send a delete user request and sign out and remove cokkies if sucsessful 
 */
async function deleteUserRequest() {
    let response = await sendRequest("DELETE", `${HOST}/api/users/${sessionStorage.getItem("username")}/`);
    if (!response.ok) {
        let alert = createAlert(`Could not delete user`);
        document.body.prepend(alert);
    } else {
        document.getElementById("user-deleted").classList.remove("hide");
        deleteCookie("access");
        deleteCookie("refresh");
        deleteCookie("remember_me");
        sessionStorage.removeItem("username");
    }
}

/**
 * Add event listeners to buttons
 */
window.addEventListener("DOMContentLoaded", async () => {
    let deleteUserButton = document.getElementById("btn-delete-user");
    deleteUserButton.addEventListener("click", showConfirmationDialog);

    let confirmButton = document.getElementById("confirm-deletion-yes");
    confirmButton.addEventListener("click", deleteUser);

    let declineDeleteButton = document.getElementById("confirm-deletion-no");
    declineDeleteButton.addEventListener("click", hideConfirmationDialog);
});

