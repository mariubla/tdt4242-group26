// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('login', (user) => {
  cy.visit('/index.html')
  cy.get("#btn-login-nav").click()
  cy.get('input[name="username"]').type(user.username)
  cy.get('input[name="password"]').type(user.password)
  cy.get('#btn-login').click()
  cy.url().should('include', 'workouts.html')
})


Cypress.Commands.add('addWorkout', (workout) => {
  cy.url().should('include', 'workouts.html')
  cy.get('#btn-create-workout').click()
  cy.url().should('include', 'workout.html')
  cy.get('input[name="name"]').type(workout.name)
  cy.get('input[name="date"]').type(workout.date)
  cy.get('select[name="visibility"]').type(workout.visibility)
  cy.get('textarea[name="notes"]').type(workout.notes)
  cy.get('#btn-ok-workout').click()
})