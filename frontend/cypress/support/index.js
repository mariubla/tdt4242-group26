// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

var user;

function generateRandomString(n=10) {
    var chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
    var string = '';
    for (var i = 0; i < n; i++) {
        string += chars[Math.floor(Math.random() * chars.length)];
    }
    return string;
}

function generateNumber(n) {
    var numbers = '1234567890';
    var number ='';
    for (var i = 0; i < n; i++) {
        number += numbers[Math.floor(Math.random() * numbers.length)];
    }
    return number;
}

function generateUserName() {
    return 'test-user-' + generateRandomString(10);
}

function generateEmail() {
    return generateRandomString(10) + '@domain.com';
}

function generateUserData() {
    return {
        username: generateUserName(),
        email: generateEmail(),
        password: generateRandomString(10),
        phone_number: generateNumber(8),
        country: generateRandomString(15),
        city: generateRandomString(10),
        street_address: generateRandomString(20)
    }
}

function registerUser(user) {
    cy.visit('/index.html')
    cy.get('#btn-register').click()
    cy.url().should('include', 'register.html')
    cy.get('input[name="email"]').type(user.email)
    cy.get('input[name="username"]').type(user.username)
    cy.get('input[name="password"]').type(user.password)
    cy.get('input[name="password1"]').type(user.password)
    cy.get('input[name="phone_number"]').type(user.phone_number)
    cy.get('input[name="country"]').type(user.country)
    cy.get('input[name="city"]').type(user.city)
    cy.get('input[name="street_address"]').type(user.street_address)
    cy.get('#btn-create-account').click()
    cy.url().should('include', 'workouts.html')
}

function logout() {
    cy.get('#btn-logout').click()
    cy.url().should('include', 'index.html')
}

before(() => {
    user = generateUserData();
    registerUser(user)
    logout()
})

beforeEach(() => {
    cy.wrap(user).as('testUser')
})

