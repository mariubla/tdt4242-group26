var workout1 = {
    name: 'Strength',
    date: '2020-06-01T08:30',
    visibility: 'PO',
    notes: 'A medium workout'
}
var workout_name_inside = {
    name: 'W',
    date: '2020-06-01T08:30',
    visibility: 'PO',
    notes: 'hei'
}
var workout_note_inside = {
    name: 'Workout',
    date: '2020-06-01T08:30',
    notes: 'h'
}
var workout_name_outside = {
    name: ' ',
    date: '2020-06-01T08:30',
    notes: 'hei'
}
var workout_notes_outside = {
    name: 'Workout',
    date: '2020-06-01T08:30',
    notes: ' '
}

var exercise = {
    sets: '10',
    number:'10'
}

var exercise_inside = {
    sets: '1',
    number: '1'
}

var exercise_outside = {
    sets: ' ',
    number: ' '
}

var workout2 = {
    name: 'Running',
    date: '2020-07-01T08:30',
    visibility: 'PO',
    notes: 'A good workout'
}
var workout3 = {
    name: 'test-workout-1',
    date: '2020-07-01T08:30',
    visibility: 'PO',
    notes: 'Good walk'
}


describe('View workout', () => {
    it('Show all field', function () {
        cy.login(this.testUser)
        cy.addWorkout(workout1)
        cy.addWorkout(workout2)
        cy.addWorkout(workout3)
        cy.url().should('include', 'workouts.html')
        cy.visit('/workout.html?id=1')
        cy.url().should('include', 'workout.html')
        cy.get('input[name="name"]')
        cy.get('input[name="date"]')
        cy.get('select[name="visibility"]')
        cy.get('textarea[name="notes"]')
    })
})

describe('Boundary name', () => {
    it('should first fail, then add workout', function () {
        cy.login(this.testUser)
        cy.visit('/workouts.html')
        cy.get("#btn-create-workout").click()
        cy.url().should('include', 'workout.html')
        cy.get('input[name="name"]').type(workout_name_outside.name)
        cy.get('input[name="date"]').type(workout_name_inside.date)
        cy.get('textarea[name="notes"]').type(workout_name_inside.notes)
        cy.get('#btn-ok-workout').click()
        cy.url().should('include', 'workout.html')
        cy.get('input[name="name"]').type(workout_name_inside.name)
        cy.get('#btn-ok-workout').click()
        cy.url().should('include', 'workouts.html')
    })
})

describe('Boundary notes ', () => {
    it('should first fail, then add workout', function () {
        cy.login(this.testUser)
        cy.visit('/workouts.html')
        cy.get("#btn-create-workout").click()
        cy.url().should('include', 'workout.html')
        cy.get('input[name="name"]').type(workout_note_inside.name)
        cy.get('input[name="date"]').type(workout_note_inside.date)
        cy.get('textarea[name="notes"]').type(workout_notes_outside.notes)
        cy.get('#btn-ok-workout').click()
        cy.url().should('include', 'workout.html')
        cy.get('textarea[name="notes"]').type(workout_note_inside.notes)
        cy.get('#btn-ok-workout').click()
        cy.url().should('include', 'workouts.html')
    })
})

describe('Boundary exercise sets', () => {
    it('should first fail, then add workout', function () {
        cy.login(this.testUser)
        cy.visit('/workouts.html')
        cy.get("#btn-create-workout").click()
        cy.url().should('include', 'workout.html')
        cy.get('input[name="name"]').type(workout.name)
        cy.get('input[name="date"]').type(workout.date)
        cy.get('textarea[name="notes"]').type(workout.notes)
        cy.get('select[name="type"]').select('1').should('have.value', '1')
        cy.get('input[name="sets"]').type(exercise_outside.sets)
        cy.get('input[name="number"]').type(exercise.number)
        cy.get('#btn-ok-workout').click()
        cy.url().should('include', 'workout.html')
        cy.get('input[name="sets"]').type(exercise_inside.sets)
        cy.get('#btn-ok-workout').click()
        cy.url().should('include', 'workouts.html')
    })
})

describe('Boundary exercise number', () => {
    it('should first fail, then add workout', function () {
        cy.login(this.testUser)
        cy.visit('/workouts.html')
        cy.get("#btn-create-workout").click()
        cy.url().should('include', 'workout.html')
        cy.get('input[name="name"]').type(workout.name)
        cy.get('input[name="date"]').type(workout.date)
        cy.get('textarea[name="notes"]').type(workout.notes)
        cy.get('select[name="type"').select('1').should('have.value', '1')
        cy.get('input[name="sets"]').type(exercise.sets)
        cy.get('input[name="number"]').type(exercise_outside.number)
        cy.get('#btn-ok-workout').click()
        cy.url().should('include', 'workout.html')
        cy.get('input[name="number"]').type(exercise_inside.number)
        cy.get('#btn-ok-workout').click()
        cy.url().should('include', 'workouts.html')
    })
})

describe('Delete user', () => {
    it('Should delete user', function () {
        cy.login(this.testUser)
        cy.visit('/deleteuser.html')
        cy.get('#btn-delete-user').click()
        cy.contains('Confirm deletion of user')
        cy.get('#confirm-deletion-yes').click()
        cy.visit('/index.html')
        cy.get("#btn-login-nav").click()
        cy.get('input[name="username"]').type(this.testUser.username)
        cy.get('input[name="password"]').type(this.testUser.password)
        cy.get('#btn-login').click()
        cy.contains('Login failed!')
    })
})

describe('Search workout', () => {
    it('Find correct workout', function () {
        cy.login(this.testUser)
        cy.get('#search-workouts-field').type('Running')
        cy.get('#search-workouts').click()
        cy.get('.mb-1').contains('Running')
    })
})
